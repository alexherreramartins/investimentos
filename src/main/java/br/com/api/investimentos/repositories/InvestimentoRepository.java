package br.com.api.investimentos.repositories;

import br.com.api.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento,Integer> {
}
