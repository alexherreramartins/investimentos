package br.com.api.investimentos.services;

import br.com.api.investimentos.DTOs.InvestimentoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.repositories.InvestimentoRepository;
import br.com.api.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.DecimalFormat;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    @Autowired
    SimulacaoRepository simulacaoRepository;

    public Investimento criarNovoInvestimento(Investimento investimento) {
        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> consultarTodosInvestimentos() {
        return investimentoRepository.findAll();
    }

    public InvestimentoDTO criarNovaSimulacao(Simulacao simulacao, int idinvestimentos) {
        DecimalFormat f = new DecimalFormat("#.##");
        Optional<Investimento> investimento = investimentoRepository.findById(idinvestimentos);

        if (investimento.isPresent()) {
            Double montanteFinal = simulacao.getValorAplicado() * (1 + (investimento.get().getRendimentoAoMes() / 100) * 12);

            simulacao.setInvestimento(investimento.get());
            simulacaoRepository.save(simulacao);

            InvestimentoDTO saida = new InvestimentoDTO();

            saida.setMontante(Double.parseDouble(f.format(montanteFinal).replace(",",".")));
            saida.setRendimentoPorMes(investimento.get().getRendimentoAoMes());

            return saida;
        } else {
            throw new RuntimeException("Investimento nao cadastrado no sistema.");
        }

    }
}
