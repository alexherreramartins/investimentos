package br.com.api.investimentos.services;

import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;

    public Iterable<SimulacaoDTO> consultarTodasSimulacoes() {
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        List<SimulacaoDTO> simulacaoDTOS = new ArrayList<>();

        for (Simulacao simulacao : simulacoes){
            SimulacaoDTO simulacaoDTO = new SimulacaoDTO();

            simulacaoDTO.setEmail(simulacao.getEmail());
            simulacaoDTO.setNomeInteressado(simulacao.getEmail());
            simulacaoDTO.setQuantidadeMeses(simulacao.getQuantidadeMeses());
            simulacaoDTO.setValorAplicado(simulacao.getValorAplicado());
            simulacaoDTO.setInvestimentoID(simulacao.getInvestimento().getId());

            simulacaoDTOS.add(simulacaoDTO);
        }

        return  simulacaoDTOS;
    }

}
