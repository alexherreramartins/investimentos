package br.com.api.investimentos.models;

import javax.persistence.*;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String nome;
    private Double rendimentoAoMes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
