package br.com.api.investimentos.controllers;

import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<SimulacaoDTO> consultarTodasSimulacoes(){
        List<SimulacaoDTO> simulacaoDTOS = (List<SimulacaoDTO>) simulacaoService.consultarTodasSimulacoes();

        if (simulacaoDTOS.size() == 0){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }else{
            return simulacaoDTOS;
        }

    }



}
