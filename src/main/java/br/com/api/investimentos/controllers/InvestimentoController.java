package br.com.api.investimentos.controllers;

import br.com.api.investimentos.DTOs.InvestimentoDTO;
import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")

public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento criarNovoInvestimento(@RequestBody @Valid Investimento investimento) {
        try {
            return investimentoService.criarNovoInvestimento(investimento);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Investimento> consultarTodosInvestimentos() {

        List<Investimento> investimentos = (List<Investimento>) investimentoService.consultarTodosInvestimentos();

        if (investimentos.size() == 0){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }else{
            return investimentos;
        }

    }

    @PostMapping("/{idinvestimentos}/simulacao")
    public InvestimentoDTO criarNovaSimulacao(@RequestBody @Valid Simulacao simulacao, @PathVariable(name = "idinvestimentos") int idinvestimentos) {
        try {
            return investimentoService.criarNovaSimulacao(simulacao, idinvestimentos);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
