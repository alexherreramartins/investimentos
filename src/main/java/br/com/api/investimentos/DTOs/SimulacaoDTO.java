package br.com.api.investimentos.DTOs;

public class SimulacaoDTO {

    private String nomeInteressado;
    private String email;
    private double valorAplicado;
    private int quantidadeMeses;
    private int investimentoID;

    public SimulacaoDTO() {
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public int getInvestimentoID() {
        return investimentoID;
    }

    public void setInvestimentoID(int investimentoID) {
        this.investimentoID = investimentoID;
    }
}
