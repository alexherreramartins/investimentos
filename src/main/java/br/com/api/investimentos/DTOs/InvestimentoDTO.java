package br.com.api.investimentos.DTOs;

import javax.validation.Valid;
import javax.validation.constraints.Digits;

public class InvestimentoDTO {
    private Double rendimentoPorMes;
    private Double montante;

    public InvestimentoDTO() {
    }

    public Double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(Double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public Double getMontante() {
        return montante;
    }

    @Valid
    public void setMontante(Double montante) {
        this.montante = montante;
    }
}
