package br.com.api.investimentos.services;

import br.com.api.investimentos.DTOs.InvestimentoDTO;
import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.repositories.InvestimentoRepository;
import br.com.api.investimentos.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import sun.net.www.MimeTable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    @MockBean
    private SimulacaoRepository simulacaoRepository;

    private Simulacao simulacao;
    private Investimento investimento;
    private SimulacaoDTO simulacaoDTO;

    @BeforeEach
    private void init() {
        this.simulacao = new Simulacao();
        this.simulacaoDTO = new SimulacaoDTO();
        this.investimento = new Investimento();

        this.simulacao.setEmail("teste@teste.com");
        this.simulacao.setNomeInteressado("Teste");
        this.simulacao.setQuantidadeMeses(12);
        this.simulacao.setValorAplicado(5000.00);

        this.investimento.setId(1);
        this.investimento.setNome("Teste");
        this.investimento.setRendimentoAoMes(0.2);

        this.simulacao.setInvestimento(this.investimento);

        this.simulacaoDTO.setValorAplicado(this.simulacao.getValorAplicado());
        this.simulacaoDTO.setQuantidadeMeses(this.simulacao.getQuantidadeMeses());
        this.simulacaoDTO.setNomeInteressado(this.simulacao.getNomeInteressado());
        this.simulacaoDTO.setEmail(this.simulacao.getEmail());
        this.simulacaoDTO.setInvestimentoID(this.investimento.getId());

    }

    @Test
    public void TestarCriarNovoInvestimento(){

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).then(objeto -> objeto.getArgument(0));

        Assertions.assertEquals(this.investimento.getId(), investimentoService.criarNovoInvestimento(this.investimento).getId());
        Assertions.assertEquals(this.investimento.getRendimentoAoMes(), investimentoService.criarNovoInvestimento(this.investimento).getRendimentoAoMes());
        Assertions.assertEquals(this.investimento.getNome(), investimentoService.criarNovoInvestimento(this.investimento).getNome());

    }


    @Test
    public void TestarConsultarTodosInvestimentos(){

        Mockito.when(investimentoRepository.findAll()).thenReturn(Arrays.asList(this.investimento));

        List<Investimento> validacao = (List<Investimento>) investimentoService.consultarTodosInvestimentos();

        Assertions.assertEquals(this.investimento.getId(), validacao.get(0).getId());

    }

    @Test
    public void TestarCriarSimulacaoProdutoExistente(){

        Mockito.when(investimentoRepository.findById(this.investimento.getId())).thenReturn(Optional.ofNullable(this.investimento));

        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).then(objeto -> objeto.getArgument(0));

        InvestimentoDTO investimentoDTO = investimentoService.criarNovaSimulacao(this.simulacao,this.investimento.getId());

        Double montanteFinal = this.simulacao.getValorAplicado() * (1 + (this.investimento.getRendimentoAoMes() / 100) * 12);

        Assertions.assertEquals(montanteFinal, investimentoDTO.getMontante());

    }

    @Test
    public void TestarCriarSimulacaoProdutoNaoExistente(){

        Mockito.when(investimentoRepository.findById(this.investimento.getId())).thenReturn(null);

        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.criarNovaSimulacao(this.simulacao,this.investimento.getId());});
    }
}
