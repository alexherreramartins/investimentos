package br.com.api.investimentos.services;

import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTeste {
    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    private Simulacao simulacao;
    private Investimento investimento;
    private SimulacaoDTO simulacaoDTO;

    @BeforeEach
    private void init() {
        this.simulacao = new Simulacao();
        this.simulacaoDTO = new SimulacaoDTO();
        this.investimento = new Investimento();

        this.simulacao.setEmail("teste@teste.com");
        this.simulacao.setNomeInteressado("Teste");
        this.simulacao.setQuantidadeMeses(12);
        this.simulacao.setValorAplicado(5000.00);

        this.investimento.setId(1);
        this.investimento.setNome("Teste");
        this.investimento.setRendimentoAoMes(0.2);

        this.simulacao.setInvestimento(this.investimento);

        this.simulacaoDTO.setValorAplicado(this.simulacao.getValorAplicado());
        this.simulacaoDTO.setQuantidadeMeses(this.simulacao.getQuantidadeMeses());
        this.simulacaoDTO.setNomeInteressado(this.simulacao.getNomeInteressado());
        this.simulacaoDTO.setEmail(this.simulacao.getEmail());
        this.simulacaoDTO.setInvestimentoID(this.investimento.getId());

    }

    @Test
    public void TestarBuscarSimulacoes(){

        Optional<Simulacao> simulacaoOptional = Optional.of(this.simulacao);

        Mockito.when(simulacaoRepository.findAll()).thenReturn(Arrays.asList(this.simulacao));

        List<SimulacaoDTO> validacao = (List<SimulacaoDTO>) simulacaoService.consultarTodasSimulacoes();

        Assertions.assertEquals(simulacaoDTO.getInvestimentoID(), validacao.get(0).getInvestimentoID());

    }
}
