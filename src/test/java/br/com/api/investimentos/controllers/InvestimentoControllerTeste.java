package br.com.api.investimentos.controllers;

import br.com.api.investimentos.DTOs.InvestimentoDTO;
import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.services.InvestimentoService;
import br.com.api.investimentos.services.SimulacaoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collections;


@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTeste {

    @MockBean
    SimulacaoService simulacaoService;

    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    private Simulacao simulacao;
    private Investimento investimento;
    private SimulacaoDTO simulacaoDTO;
    private InvestimentoDTO investimentoDTO;

    @BeforeEach
    private void init() {
        this.simulacao = new Simulacao();
        this.simulacaoDTO = new SimulacaoDTO();
        this.investimento = new Investimento();
        this.investimentoDTO = new InvestimentoDTO();

        this.simulacao.setEmail("teste@teste.com");
        this.simulacao.setNomeInteressado("Teste");
        this.simulacao.setQuantidadeMeses(12);
        this.simulacao.setValorAplicado(5000.00);

        this.investimento.setId(1);
        this.investimento.setNome("Teste");
        this.investimento.setRendimentoAoMes(0.2);

        this.simulacao.setInvestimento(this.investimento);

        this.simulacaoDTO.setValorAplicado(this.simulacao.getValorAplicado());
        this.simulacaoDTO.setQuantidadeMeses(this.simulacao.getQuantidadeMeses());
        this.simulacaoDTO.setNomeInteressado(this.simulacao.getNomeInteressado());
        this.simulacaoDTO.setEmail(this.simulacao.getEmail());
        this.simulacaoDTO.setInvestimentoID(this.investimento.getId());


    }

    @Test
    public void testarCriarNovoInvestimento() throws Exception {
        Mockito.when(investimentoService.criarNovoInvestimento(Mockito.any(Investimento.class)))
                .thenReturn((this.investimento));
        ObjectMapper objectMapper = new ObjectMapper();

        String investimentoJson = objectMapper.writeValueAsString(this.investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON).content(investimentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(this.investimento.getId())));

    }

    @Test
    public void testarConsultarTodosInvestimentos() throws Exception {
        Mockito.when(investimentoService.consultarTodosInvestimentos())
                .thenReturn(Arrays.asList(this.investimento));

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());

    }

    @Test
    public void testarSimularInvestimento() throws Exception {

        Double montanteFinal = this.simulacao.getValorAplicado() * (1 + (this.investimento.getRendimentoAoMes() / 100) * 12);
        this.investimentoDTO.setMontante(montanteFinal);
        this.investimentoDTO.setRendimentoPorMes(this.investimento.getRendimentoAoMes());

        Mockito.when(investimentoService.criarNovaSimulacao(Mockito.any(Simulacao.class), Mockito.anyInt())).thenReturn(this.investimentoDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String simulacaoJson = objectMapper.writeValueAsString(this.simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.montante",
                        CoreMatchers.equalTo(this.investimentoDTO.getMontante())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rendimentoPorMes",
                        CoreMatchers.equalTo(this.investimentoDTO.getRendimentoPorMes())));
    }


    @Test
    public void testarCriarNovoInvestimentoRuntimeService() throws Exception {
        Mockito.when(investimentoService.criarNovoInvestimento(Mockito.any(Investimento.class)))
                .thenThrow(RuntimeException.class);

        ObjectMapper objectMapper = new ObjectMapper();

        String investimentoJson = objectMapper.writeValueAsString(this.investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON).content(investimentoJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarSimularInvestimentoRuntimeService() throws Exception {

        Double montanteFinal = this.simulacao.getValorAplicado() * (1 + (this.investimento.getRendimentoAoMes() / 100) * 12);
        this.investimentoDTO.setMontante(montanteFinal);
        this.investimentoDTO.setRendimentoPorMes(this.investimento.getRendimentoAoMes());

        Mockito.when(investimentoService.criarNovaSimulacao(Mockito.any(Simulacao.class), Mockito.anyInt()))
                .thenThrow(RuntimeException.class);

        ObjectMapper objectMapper = new ObjectMapper();
        String simulacaoJson = objectMapper.writeValueAsString(this.simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarConsultarTodosInvestimentosNoContent() throws Exception {
        Mockito.when(investimentoService.consultarTodosInvestimentos())
                .thenReturn(Collections.EMPTY_LIST);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }

    @Test
    public void testarSimularInvestimentoEmailInvalido() throws Exception {

        Double montanteFinal = this.simulacao.getValorAplicado() * (1 + (this.investimento.getRendimentoAoMes() / 100) * 12);
        this.investimentoDTO.setMontante(montanteFinal);
        this.investimentoDTO.setRendimentoPorMes(this.investimento.getRendimentoAoMes());

        Mockito.when(investimentoService.criarNovaSimulacao(Mockito.any(Simulacao.class), Mockito.anyInt()))
                .thenThrow(RuntimeException.class);

        ObjectMapper objectMapper = new ObjectMapper();

        this.simulacao.setEmail("hfdusfhusfsofsf");

        String simulacaoJson = objectMapper.writeValueAsString(this.simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

    }

    @Test
    public void testarSimularInvestimentoMontanteInvalido() throws Exception {

        Double montanteFinal = this.simulacao.getValorAplicado() * (1 + (this.investimento.getRendimentoAoMes() / 100) * 12);
        this.investimentoDTO.setMontante(montanteFinal);
        this.investimentoDTO.setRendimentoPorMes(this.investimento.getRendimentoAoMes());

        Mockito.when(investimentoService.criarNovaSimulacao(Mockito.any(Simulacao.class), Mockito.anyInt()))
                .thenThrow(RuntimeException.class);

        ObjectMapper objectMapper = new ObjectMapper();

        this.simulacao.setValorAplicado(1000.03874);

        String simulacaoJson = objectMapper.writeValueAsString(this.simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

    }

}
