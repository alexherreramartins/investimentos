package br.com.api.investimentos.controllers;

import br.com.api.investimentos.DTOs.SimulacaoDTO;
import br.com.api.investimentos.models.Investimento;
import br.com.api.investimentos.models.Simulacao;
import br.com.api.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTeste {
    @MockBean
    SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    private Simulacao simulacao;
    private Investimento investimento;
    private SimulacaoDTO simulacaoDTO;

    @BeforeEach
    private void init() {
        this.simulacao = new Simulacao();
        this.simulacaoDTO = new SimulacaoDTO();
        this.investimento = new Investimento();

        this.simulacao.setEmail("teste@teste.com");
        this.simulacao.setNomeInteressado("Teste");
        this.simulacao.setQuantidadeMeses(12);
        this.simulacao.setValorAplicado(5000.00);

        this.investimento.setId(1);
        this.investimento.setNome("Teste");
        this.investimento.setRendimentoAoMes(0.2);

        this.simulacao.setInvestimento(this.investimento);

        this.simulacaoDTO.setValorAplicado(this.simulacao.getValorAplicado());
        this.simulacaoDTO.setQuantidadeMeses(this.simulacao.getQuantidadeMeses());
        this.simulacaoDTO.setNomeInteressado(this.simulacao.getNomeInteressado());
        this.simulacaoDTO.setEmail(this.simulacao.getEmail());
        this.simulacaoDTO.setInvestimentoID(this.investimento.getId());

    }

    @Test
    public void testarBuscarSimulacoes() throws Exception {
        Mockito.when(simulacaoService.consultarTodasSimulacoes()).thenReturn(Arrays.asList(this.simulacaoDTO));

        mockMvc.perform(MockMvcRequestBuilders.get("/simulacoes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());

    }


    @Test
    public void testarBuscarSimulacoesSemConteudo() throws Exception {
        Mockito.when(simulacaoService.consultarTodasSimulacoes()).thenReturn(Collections.EMPTY_LIST);

        mockMvc.perform(MockMvcRequestBuilders.get("/simulacoes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }
}
